package com.example.android.google.wearable.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.parse.ParseUser;


public class AccountSetup extends Activity {

    ParseUser  user;
    EditText   etInterest;
    Button     btnSave;
    Button     btnCancel;
    RadioGroup rgGender;
    CheckBox   cbMale;
    CheckBox   cbFemale;
    CheckBox   cbMtF;
    CheckBox   cbFtM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setup);
        setWidgets();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setWidgets() {
        user       = ParseUser.getCurrentUser();
        etInterest = (EditText) findViewById(R.id.etInterests);
        btnSave    = (Button) findViewById(R.id.btnSave);
        btnCancel  = (Button) findViewById(R.id.btnCancel);
        rgGender   = (RadioGroup) findViewById(R.id.rgGender);
        cbMale     = (CheckBox) findViewById(R.id.cbMale);
        cbFemale   = (CheckBox) findViewById(R.id.cbFemale);
        cbMtF      = (CheckBox) findViewById(R.id.cbMtF);
        cbFtM      = (CheckBox) findViewById(R.id.cbFtM);

        if (user.has("Gender")) {
            int count = rgGender.getChildCount();
            String gender = user.get("Gender").toString();
            for (int i = 0; i < count; i++) {
                RadioButton btn = (RadioButton) rgGender.getChildAt(i);
                if (gender.equals(btn.getText().toString())) {
                    btn.setSelected(true);
                }
            }
        }
        if (user.has("LookingForMales")) {
            cbMale.setSelected(true);
        }
        if (user.has("LookingForFemales")) {
            cbFemale.setSelected((true));
        }
        if (user.has("LookingForMtF")) {
            cbMtF.setSelected(true);
        }
        if (user.has("LookingForFtM")) {
            cbFtM.setSelected(true);
        }
        if (user.has("Interests")) {
            etInterest.setText(user.get("Interests").toString());
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInterests();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelProgress();
            }
        });
    }

    private boolean validSetup() {
        boolean isValid = true;
        if (etInterest.getText().toString().isEmpty()) {
            etInterest.setError("Interests must be filled.");
            isValid = false;
        }

        return isValid;
    }

    private String getGender() {
        int id = rgGender.getCheckedRadioButtonId();
        RadioButton btn = (RadioButton) findViewById(id);

        return btn.getText().toString();
    }

    private void saveInterests() {
        if (validSetup()) {
            user.put("Interests", etInterest.getText().toString());
            user.put("Gender", getGender());
            user.put("LookingForMales", cbMale.isChecked());
            user.put("LookingForFemales", cbFemale.isChecked());
            user.put("LookingForMtF", cbMtF.isChecked());
            user.put("LookingForFtM", cbFtM.isChecked());
            user.saveInBackground();
            Intent intent = new Intent(getApplicationContext(), ViewPostsActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void cancelProgress() {
        finish();
    }
}
