package com.example.android.google.wearable.app;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;

import android.app.Dialog;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseACL;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.Date;

public class ViewPostsActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeLayout;
    private Handler handler;
    private ParseQueryAdapter adapter;
    private ListView listView;

    private LocationHandler locationHandler;

    private final int RANGE = 100;

    private boolean isRefreshing;

    public Context context;

    public String username;

    String msgToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_posts);
        setTitle("Locals");
        handler = new Handler();

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setSize(SwipeRefreshLayout.LARGE);
        swipeLayout.setSoundEffectsEnabled(true);

        listView = (ListView) findViewById(R.id.posts_list);

        locationHandler = new LocationHandler(this);

        populateList();

        registerListener();

        context = this;

    }


    private void registerListener()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView< ?> parent, View view, int position, long id)
            {
                final Post item = (Post) adapter.getItem(position);

                 username = item.getUser().getUsername();

                Intent intent = new Intent(getBaseContext(), SendNotification.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

    }


    private void populateList() {

        isRefreshing = true;

        ParseQueryAdapter.QueryFactory<Post> factory =
                new ParseQueryAdapter.QueryFactory<Post>() {
                    public ParseQuery<Post> create() {
                        ParseQuery<Post> query = Post.getQuery();
                        // TODO: Pass location into query
                        query.include("user");
                        query.whereWithinKilometers("location", locationHandler.getLocation(), RANGE);
                        Date currentDate = new Date();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(currentDate);
                        calendar.add(Calendar.HOUR, -2);
                        Date twoHoursBack = calendar.getTime();
                        query.whereGreaterThan("createdAt", twoHoursBack);
                        query.orderByDescending("createdAt");
                        return query;
                    }
                };

        adapter = new ParseQueryAdapter<Post>(this, factory) {
            @Override
            public View getItemView(Post post, View view, ViewGroup parent) {
                if (view == null) {
                    view = View.inflate(getContext(), R.layout.post_list_item, null);
                }
                ParseUser user = post.getUser();
                TextView usernameView = (TextView) view.findViewById(R.id.post_username);
                usernameView.setText(user.getUsername());
                TextView activityView = (TextView) view.findViewById(R.id.post_activity);
                activityView.setText(post.getActivity());
                return view;
            }
        };

        isRefreshing = false;

        listView.setAdapter(adapter);

    }

    @Override
    public void onRefresh() {
        populateList();
        handler.post(refreshing);
    }

    private final Runnable refreshing = new Runnable(){
        public void run(){
            try {
                if(isRefreshing){
                    // re run the verification after 1 second
                    handler.postDelayed(this, 1000);
                }else{
                    // stop the animation after the data is fully loaded
                    adapter.notifyDataSetChanged();
                    swipeLayout.setRefreshing(false);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_posts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.create_post_menu_btn) {
            // TODO: Open post dialog
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_create_post);
            dialog.setTitle("New Post");

            Button cancelButton = (Button) dialog.findViewById(R.id.create_post_cancel);
            Button confirmButton = (Button) dialog.findViewById(R.id.create_post_confirm);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText postMessage = (EditText) dialog.findViewById(R.id.post_message);
                    String message = postMessage.getText().toString();
                    ParseGeoPoint location = locationHandler.getLocation();
                    post(message, location);
                    dialog.dismiss();
                }
            });

            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void post(String message, ParseGeoPoint location) {
        Post post = new Post();
        post.setLocation(location);
        post.setUser(ParseUser.getCurrentUser());
        post.setActivity(message);
        ParseACL acl = new ParseACL();
        acl.setPublicReadAccess(true);
        post.setACL(acl);
        post.saveInBackground();
    }
}