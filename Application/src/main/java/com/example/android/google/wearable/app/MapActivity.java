package com.example.android.google.wearable.app;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Activity {
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
            LatLng latlng = extras.getParcelable("LatLong");
            LocationFromList( latlng );
            //setUpMap();
        }
        else
        {
            setUpMap();
        }

    }

    private void LocationFromList( LatLng latLng ) {

        map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker").snippet("Snippet"));

        // Enable MyLocation Layer of Google Map
        map.setMyLocationEnabled(true);

        // Get LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Get Current Location
        Location myLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        // set map type
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        map.animateCamera(CameraUpdateFactory.zoomTo(16));

    }

    private void setUpMap() {

        map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker").snippet("Snippet"));

        // Enable MyLocation Layer of Google Map
        map.setMyLocationEnabled(true);

        // Get LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Get Current Location
        Location myLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        // set map type
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        double latitude;
        double longitude;
        try {
            latitude = myLocation.getLatitude();
            longitude = myLocation.getLongitude();

            // Create a LatLng object for the current location
            LatLng latLng = new LatLng(latitude, longitude);

            // Show the current location in Google Map
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        } catch (NullPointerException e) {
            Log.e("Location", "Error getting location");
            Toast.makeText(getApplicationContext(), "Error getting location", Toast.LENGTH_SHORT).show();

            LatLng latLng = new LatLng(49.2511975, -123.12096220000001);

            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }

        // Zoom in the Google Map
        map.animateCamera(CameraUpdateFactory.zoomTo(14));
        //map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("You are here!").snippet("Consider yourself located"));
    }
}