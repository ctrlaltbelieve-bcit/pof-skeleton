package com.example.android.google.wearable.app;

import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Created by brycen on 15-03-10.
 */
public class Application extends android.app.Application {

    private static Application singletonInstance;

    public void onCreate() {
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "36GC7FZhLhLBayX6LLvufP0vTq5HrNptbIbDnvTX", "vLsnwMzMLtgULozHAukMKRIodMfu90W4n51PzSku");
        ParseObject.registerSubclass(Post.class);

        singletonInstance = this;
    }

    public static synchronized Application getInstance() {
        return singletonInstance;
    }
}
