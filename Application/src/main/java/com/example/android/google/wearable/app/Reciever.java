package com.example.android.google.wearable.app;

import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

/**
 * Created by sparqy on 11/03/15.
 */
public class Reciever  extends ParsePushBroadcastReceiver
{
    @Override
    public void onPushOpen(Context context, Intent intent)
    {
        Intent i = new Intent(context, SendDemand.class);


        i.putExtras(intent.getExtras());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
