package com.example.android.google.wearable.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;


public class Login extends Activity {

    EditText etUsername;
    EditText etPassword;
    Button   btnSignIn;
    Button   btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Initialize UI
        etUsername  = (EditText) findViewById(R.id.etUsername);
        etPassword  = (EditText) findViewById(R.id.etPasword);
        btnSignIn   = (Button) findViewById(R.id.btnSignIn);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        // Attach listeners to buttons.
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        if (checkUserLoggedIn()) {
            Intent intent = new Intent(getApplicationContext(), ViewPostsActivity.class);
            startActivity(intent);
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void signIn() {
        final String username = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e != null) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
                else {
                    if (parseUser.getCurrentUser() != null) {

                        //Register device as signed in user
                        // Associate the device with a user
                        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                        installation.put("username", username);
                        installation.saveInBackground();

                        Intent intent = new Intent(getApplicationContext(), ViewPostsActivity.class);
                        startActivity(intent);

                        finish();
                    }
                    else {
                        Intent intent = new Intent(getApplicationContext(), CentralActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }
            }
        });


    }

    private void register() {
        Intent intent = new Intent(getApplicationContext(), Register.class);
        Bundle bundle = new Bundle();
        String username = etUsername.getText().toString().trim();
        bundle.putString("Username", username);
        intent.putExtras(bundle);
        startActivity(intent);
        this.finish();

    }

    private boolean checkUserLoggedIn() {
        ParseUser user = ParseUser.getCurrentUser();
        return (user != null);
    }
}
