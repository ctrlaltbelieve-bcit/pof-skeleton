package com.example.android.google.wearable.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.Calendar;
import java.util.regex.Pattern;


public class Register extends Activity {

    EditText   etFirstName;
    EditText   etLastName;
    EditText   etUsername;
    EditText   etEmail;
    EditText   etPassword;
    EditText   etPasswordConf;
    DatePicker dpBirthday;
    Button     btnRegister;
    Bundle     bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setWidgets();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setWidgets() {
        // Initialize user entry points.
        etFirstName    = (EditText) findViewById(R.id.etFirstName);
        etLastName     = (EditText) findViewById(R.id.etLastName);
        etUsername     = (EditText) findViewById(R.id.etUsername);
        etEmail        = (EditText) findViewById(R.id.etEmail);
        etPassword     = (EditText) findViewById(R.id.etPassword);
        etPasswordConf = (EditText) findViewById(R.id.etPasswordConf);
        dpBirthday     = (DatePicker) findViewById(R.id.dpBirthday);
        btnRegister    = (Button) findViewById(R.id.btnRegister);

        // Attach listener to button.
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        // Get bundles
        bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("Username")) {
            String username = bundle.getString("Username");
            if (!username.isEmpty()) {
                etUsername.setText(username);
            }
        }
    }

    private void register() {
        boolean validLogin;
        String strFirstName    = etFirstName.getText().toString().trim();
        String strLastName     = etLastName.getText().toString().trim();
        String strUsername     = etUsername.getText().toString().trim();
        String strEmail        = etEmail.getText().toString().trim();
        String strPassword     = etPassword.getText().toString().trim();
        String strPasswordConf = etPasswordConf.getText().toString().trim();
        int iYear  = dpBirthday.getYear();
        int iMonth = dpBirthday.getMonth();
        int iDay   = dpBirthday.getDayOfMonth();

        resetErrors();
        validateName(strFirstName, strLastName);
        validateUsername(strUsername);
        validateEmail(strEmail);
        validatePassword(strPassword, strPasswordConf);
        validateBirthday(iYear, iMonth, iDay);
        validLogin = checkErrors();

        if (validLogin) {
            ParseUser user = new ParseUser();
            user.put("FirstName", strFirstName);
            user.put("LastName", strLastName);
            user.setEmail(strEmail);
            user.setUsername(strUsername);
            user.setPassword(strPassword);
            user.put("Birthday", dateToUnix(iYear, iMonth, iDay));
            // Call the Parse signup method
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    else {
                        Intent intent = new Intent(getApplicationContext(), AccountSetup.class);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }
    }

    private void resetErrors() {
        etFirstName.setError(null);
        etLastName.setError(null);
        etUsername.setError(null);
        etEmail.setError(null);
        etPassword.setError(null);
        etPasswordConf.setError(null);
    }

    private boolean validateName(String fName, String lName) {
        boolean isValid = true;
        if (fName.isEmpty()) {
            etFirstName.setError(getString(R.string.lr_firstname_empty));
            isValid = false;
        }
        if (!Pattern.matches("[A-Za-z]+", fName)) {
            etFirstName.setError(getString(R.string.lr_firstname_has_nums));
            isValid = false;
        }
        if (lName.isEmpty()) {
            etLastName.setError(getString(R.string.lr_lastname_empty));
            isValid = false;
        }
        if (!Pattern.matches("[A-Za-z]+", lName)) {
            etLastName.setError(getString(R.string.lr_lastname_has_nums));
            isValid = false;
        }

        return isValid;
    }

    private boolean validateUsername(String username) {
        boolean isValid = true;
        if (username.isEmpty()) {
            etUsername.setError(getString(R.string.lr_username_empty));
            isValid = false;
        }

        return isValid;
    }

    private boolean validateEmail(String email) {
        boolean isValid = true;
        if (email.isEmpty()) {
            etEmail.setError(getString(R.string.lr_email_empty));
            isValid = false;
        }
        if (!Pattern.matches("^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Za-z]{2,4}$", email)) {
            etEmail.setError(getString(R.string.lr_email_not_valid));
            isValid = false;
        }

        return isValid;
    }

    private boolean validatePassword(String password, String passwordConf) {
        boolean isValid = true;
        if (password.isEmpty()) {
            etPassword.setError(getString(R.string.lr_password_empty));
            isValid = false;
        }
        if (passwordConf.isEmpty()) {
            etPasswordConf.setError(getString(R.string.lr_password_empty));
            isValid = false;
        }
        if (!password.equals(passwordConf)) {
            etPassword.setError(getString(R.string.lr_password_mismatch));
            isValid = false;
        }

        return isValid;
    }

    private boolean validateBirthday(int year, int month, int day) {
        boolean isValid = true;
        long timestamp = dateToUnix(year, month, day);
        long currentTime = System.currentTimeMillis() / 1000L;
        if (currentTime - timestamp < dateToUnix(1988, 0, 0)) {
            Toast.makeText(getApplicationContext(), "Too young", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "Old enough", Toast.LENGTH_LONG).show();
        }

        return isValid;
}

    private boolean checkErrors() {
        boolean isValid;

        isValid = (etFirstName.getError() == null
                && etLastName.getError() == null
                && etUsername.getError() == null
                && etPassword.getError() == null
                && etPassword.getError() == null);

        return isValid;
    }

    private long dateToUnix(int year, int month, int day) {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return (int) (c.getTimeInMillis() / 1000L);
    }
}
