package com.example.android.google.wearable.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.RemoteInput;
import android.util.Log;

import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;

/**
 * Created by sparqy on 11/03/15.
 */
public class DemandIntentReciever extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {


        if (intent.getAction().equals(SendDemand.ACTION_DEMAND)) {
            String message =
                intent.getStringExtra(SendDemand.EXTRA_MESSAGE);
            Log.v("MyTag", "Extra message from intent = " + message);
            Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
            CharSequence reply = remoteInput.getCharSequence(SendDemand.EXTRA_VOICE_REPLY);
            Log.v("MyTag", "User reply from wearable: " + reply);

            // Create our Installation query
            ParseQuery pushQuery = ParseInstallation.getQuery();
            pushQuery.whereEqualTo("username", "brycen");

            // Send push notification to query
            ParsePush push = new ParsePush();
            push.setQuery(pushQuery); // Set our Installation query
            push.setMessage(reply.toString());
            push.sendInBackground();

        }

    }

}